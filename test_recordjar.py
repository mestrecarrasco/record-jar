import unittest
from recordjar import parse

class TestRecordJarParsing(unittest.TestCase):
    def test_canonical_sample(self):
        filename = "canonical.sample"
        with open(filename) as f:
            content = f.read()
        returned_answer = parse(content)

        correct_answer = [{ 
              'Planet': 'Mercury', 
              'Orbital-Radius': '57,910,000 km', 
              'Diameter': '4,880 km', 
              'Mass': '3.30e23 kg'
            }, { 
              'Planet': 'Venus', 
              'Orbital-Radius': '108,200,000 km', 
              'Diameter': '12,103.6 km', 
              'Mass': '4.869e24 kg'
            }, {
               'Planet': 'Earth', 
               'Orbital-Radius': '149,600,000 km', 
               'Diameter': '12,756.3 km', 
               'Mass': '5.972e24 kg', 
               'Moons': 'Luna'
            }]

        self.assertEqual(returned_answer, correct_answer)

    def test_books_sample(self):
        filename = "books.sample"
        with open(filename) as f:
            content = f.read()
        returned_answer = parse(content, '--')

        correct_answer = [{
               'Name': 'Neuromancer', 
               'Author': 'Willian Gibson', 
               'Year': '1983', 
               'Comment': "The cyberpunk's most proeminent book -- a must read!"
            }, {
               'Name': 'Solaris', 
               'Author': 'Stanislaw Lem', 
               'Year': '1961', 
               'Comment': 'What if an -- entire -- world was a single living being?'
            }, {
               'Name': 'Foundation', 
               'Author': 'Isaac Asimov', 
               'Year': '1951', 
               'Comment': "Hollywood is adapting it to a movie series -- I don't think it will work."
            }]

        self.assertEqual(returned_answer, correct_answer)

    def test_whitespacey_sample(self):
        filename = "whitespacey.sample"
        with open(filename) as f:
            content = f.read()
        returned_answer = parse(content)

        correct_answer = [{
               'Movie': 'Interstellar', 
               'Director': 'Christopher Nolan', 
               'Year': '2014', 
            }, {
               'Movie': 'Gravity', 
               'Director': 'Alfonso Cuarón', 
               'Year': '2013', 
            }]

        self.assertEqual(returned_answer, correct_answer)

    def test_store_sample(self):
        filename = "store.sample"
        with open(filename) as f:
            content = f.read()
        returned_answer = parse(content)

        correct_answer = [{
               'Name': 'Conan\'s Savage Sword', 
               'Price': '199.99', 
               'Tags': ['weapon', 'antique', 'baddass']
            }, {
               'Name': 'Ono-sendai', 
               'Price': '399.99', 
               'Tags': 'electronics', 
            }, {
                'Name': 'Toothpick',
                'Price': '0.99'
            }]

        self.assertEqual(returned_answer, correct_answer)

    def test_lonely_sample(self):
        filename = "lonely.sample"
        with open(filename) as f:
            content = f.read()
        returned_answer = parse(content)

        correct_answer = [{
            'Name': 'Forever alone',
            'Description': 'The one who always ends alone in parties'
        }]

        self.assertEqual(returned_answer, correct_answer)

    def test_empty_string(self):
        self.assertEqual(parse(''), [])

    def test_wrong_types(self):
        self.assertRaises(TypeError, parse, 2)
        self.assertRaises(TypeError, parse, 3j)
        self.assertRaises(TypeError, parse, 4.5)
        self.assertRaises(TypeError, parse, [])
        self.assertRaises(TypeError, parse, {})
        self.assertRaises(TypeError, parse, True)

        self.assertRaises(TypeError, parse, "", 2)
        self.assertRaises(TypeError, parse, "", 3j)
        self.assertRaises(TypeError, parse, "", 4.5)
        self.assertRaises(TypeError, parse, "", [])
        self.assertRaises(TypeError, parse, "", {})
        self.assertRaises(TypeError, parse, "", True)
