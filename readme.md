# Record-Jar

This project implements a simple parser for the Record Jar format, proposed by Eric Raymond at [The Art of Unix Programming](http://catb.org/esr/writings/taoup/html/ch05s02.html). The Record Jar is a data file format similar to [RFC 822](https://tools.ietf.org/html/rfc822), but not quite the same. Here is an example:

```
Planet: Mercury
Orbital-Radius: 57,910,000 km
Diameter: 4,880 km
Mass: 3.30e23 kg
%%
Planet: Venus
Orbital-Radius: 108,200,000 km
Diameter: 12,103.6 km
Mass: 4.869e24 kg
%%
Planet: Earth
Orbital-Radius: 149,600,000 km
Diameter: 12,756.3 km
Mass: 5.972e24 kg
Moons: Luna
```

To parse it:

```python
from recordjar import parse

content = """
Planet: Mercury
Orbital-Radius: 57,910,000 km
Diameter: 4,880 km
Mass: 3.30e23 kg
%%
Planet: Venus
Orbital-Radius: 108,200,000 km
Diameter: 12,103.6 km
Mass: 4.869e24 kg
%%
Planet: Earth
Orbital-Radius: 149,600,000 km
Diameter: 12,756.3 km
Mass: 5.972e24 kg
Moons: Luna"""

print(parse(content))

```

Output:

```
[{'Planet': 'Mercury', 'Orbital-Radius': '57,910,000 km', 'Diameter': '4,880 km', 'Mass': '3.30e23 kg'}, {'Planet': 'Venus', 'Orbital-Radius': '108,200,000 km', 'Diameter': '12,103.6 km', 'Mass': '4.869e24 kg'}, {'Planet': 'Earth', 'Orbital-Radius': '149,600,000 km', 'Diameter': '12,756.3 km', 'Mass': '5.972e24 kg', 'Moons': 'Luna'}]
```

Ugh! What a mess. Let's reorganize it in order to better see the results:

```
[
    {
        'Planet': 'Mercury', 
        'Orbital-Radius': '57,910,000 km', 
        'Diameter': '4,880 km', 
        'Mass': '3.30e23 kg'
    }, {
        'Planet': 'Venus', 
        'Orbital-Radius': '108,200,000 km', 
        'Diameter': '12,103.6 km', 
        'Mass': '4.869e24 kg'
    }, {
        'Planet': 'Earth', 
        'Orbital-Radius': '149,600,000 km', 
        'Diameter': '12,756.3 km', 
        'Mass': '5.972e24 kg', 
        'Moons': 'Luna'
    }
]
```

Oh, much better! And look, it represents the original input quite well!!

Whitespace before or after keys or values are simply ignored, as blank lines. The record separator `%%` is the default value, but can be overridden:

```python
parse(content, "---")
```

I personally prefer those much more.
